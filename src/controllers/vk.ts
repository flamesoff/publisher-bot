import { Requests } from '../types/requests';

export const vkController = new class VkController {
    private groupID = 0;
    private confirmationString: string = '';


    changeGruopID(id: number) {
        this.groupID = id;
    }

    authenticateServer(request: Requests.VK.Authenticate) {
        const {
            type,
            group_id
        } = request.body;

        if (type === 'confirmation')
            return this.confirmationString;
        else
            return null;
    }

    setConfirmationString(request: Requests.VK.Confirmation) {
        const { string } = request.body;
        this.confirmationString = string;
        return this.confirmationString;
    }

    async onGroupEvent(request: Requests.VK.Event) {
        const { type } = request.body;

        switch (type) {
            case 'post':
                return await this.onPost(request);
                break;
            case 'repost':
                return await this.onRepost(request);
                break;
            default:
                throw new Error('Unsupported event type.');
                break;
        }
    }


    private async onPost(request: Requests.VK.Event) {

    }
    private async onRepost(request: Requests.VK.Event) {
        
    }

};