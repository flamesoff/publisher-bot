import express, { json } from 'express';
import { networkInterfaces } from 'os';
import vk from './routes/vk';

const PORT = 9876;
const APP = express();


APP.use(json())
    .use('/vk', vk);


APP.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});