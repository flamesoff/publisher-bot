import { Router } from 'express';
import { vkController } from '../controllers/vk';
import { Requests } from '../types/requests';


const router = Router({ mergeParams: true });


router.post('/authenticate', async (request: Requests.VK.Authenticate, response, next) => {
    response.send(vkController.authenticateServer(request));
});

router.post('/confirmation', async (request: Requests.VK.Confirmation, response, next) => {
    response.send(vkController.setConfirmationString(request));
});

router.post('/', async (request: Requests.VK.Event, response, next) => {
    response.send(vkController.onGroupEvent(request));
});


export default router;