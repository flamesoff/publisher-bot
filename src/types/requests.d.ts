import { Request } from 'express';


export namespace Requests {

    export namespace VK {

        export type EventType = 'confirmation' | 'post' | 'repost' | 'comment';


        export interface Authenticate extends Request {
            body: {
                type: EventType
                group_id: 193812691
            }
        }

        export interface Confirmation extends Request  {
            body: {
                string: string
            }
        }

        export interface Event extends Request {
            body: {
                type: EventType
                object: object
                group_id: number
            }
        }

        

    }

}