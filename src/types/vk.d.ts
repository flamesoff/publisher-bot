interface WallPost {
    id: number
    owner_id: number
    from_id: number
    created_by: number
    date: number
    text: string
    reply_owner_id: number
    friends_only: number
    comments: WallPostComments
    likes: object
    reposts: object
    views: object
    post_type: number
    post_source: object
    attachments: any[]
    geo: object
    signer_id: number
    copy_history: any[]
    can_pin: number
    can_delete: number
    can_edit: number
    is_pinned: number
    marked_as_ads: number
    is_favorite: boolean
    postponed_id: number
}

interface WallPostComments {
    count: number
    can_post: number
    groups_can_post: number
    can_close: boolean
    can_open: boolean
}

type WallPostType = 'post' | 'copy' | 'reply' | 'postpone' | 'suggest';